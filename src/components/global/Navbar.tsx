import Image from 'next/image'
import Link from 'next/link'
import React from 'react'
import { Button } from '../ui/button'
import { MenuIcon } from 'lucide-react';

type Props = {}

const Navbar = (props: Props) => {
    return (
        <div className='sticky top-0 py-4 px-4 bg-black/40 backdrop-blur-lg z-[100] flex items-center border-b-[1px] justify-between'>

            {/* Section de gauche avec largeur maximale */}
            <div className='flex justify-start items-center gap-2'>
                {/* Votre contenu ici, comme le logo */}
                <p className='text-3xl font-bold'>Fu</p>
                <Image
                    width={30}
                    height={30}
                    src="/fuzzieLogo.png"
                    alt='Fuzzie Logo'
                    className='shadow-sm'
                />
                <p className='text-3xl font-bold'>zie</p>
            </div>

            {/* Section du milieu - Votre navigation */}
            <nav className='transition-opacity duration-500 ease-in-out transform opacity-0 md:opacity-100 hidden md:flex items-center gap-4'>
                <Link href="#">Products</Link>

                <Link href="#">Pricing</Link>

                <Link href="#">Clients</Link>

                <Link href="#">Resources</Link>

                <Link href="#">Documentation</Link>

                <Link href="#">Enterprise</Link>
            </nav>

            {/* Section de droite avec la même largeur maximale que la section de gauche */}
            <div className='flex items-center justify-center gap-2 content-center h-[yourDesiredHeight]'>
                <Link
                    href="/dashboard">
                    <Button className=' shadow-[inset_0_0_0_2px_#616467] text-sm px-12 py-4 rounded-full tracking-widest uppercase font-bold bg-transparent hover:bg-[#616467] hover:text-white dark:text-neutral-200 transition duration-200'>
                        Dashboard
                    </Button>
                    <MenuIcon className='md:hidden' />
                </Link>
            </div>

        </div>
    )
}

export default Navbar